/* eslint-disable import/no-webpack-loader-syntax */
// @ts-ignore
import mapboxgl from "mapbox-gl";
import React, { FunctionComponent, useEffect, useRef, useState } from "react";
import "./map.css";
import { useFindingsHandler } from "./map.hooks";

mapboxgl.accessToken =
  "pk.eyJ1IjoibWVuNzciLCJhIjoiY2tibWp3NnhoMWpidDJzcXY5NndhNWRxdSJ9.cqyyc7GRKDEMctHzHV8AIA";

const Map: FunctionComponent = () => {
  const mapContainer = useRef<HTMLDivElement | null>(null);
  const map = useRef<mapboxgl.Map | null>(null);
  const [isInitializing, setIsInitializing] = useState(true);

  const locationsSource = "locationsSource";
  const locationsLayer = "locationsLayer";

  const { geoLocations } = useFindingsHandler();

  useEffect(() => {
    if (map.current && !isInitializing) {
      const source = map.current.getSource(locationsSource);
      const layer = map.current.getLayer(locationsLayer);

      if (layer) {
        map.current.removeLayer(locationsLayer);
      }

      if (source) {
        map.current.removeSource(locationsSource);
      }

      map.current.addSource(locationsSource, {
        type: "geojson",
        data: { type: "FeatureCollection", features: geoLocations },
      });

      map.current.addLayer({
        id: locationsLayer,
        type: "circle",
        source: locationsSource,
        paint: {
          "circle-radius": 5,
          "circle-color": [
            "case",
            ["to-boolean", ["get", "fredNr"]],
            "red",
            "blue",
          ],
        },
      });

      const bounds = new mapboxgl.LngLatBounds();

      geoLocations.forEach((feature) => {
        bounds.extend((feature.geometry as any).coordinates);
      });

      map.current.fitBounds(bounds, { padding: 50 });
    }
  }, [geoLocations]);

  useEffect(() => {
    if (!map?.current || isInitializing) {
      return;
    }

    map.current.addSource(locationsSource, {
      type: "geojson",
      data: { type: "FeatureCollection", features: [] },
    });

    map.current.addLayer({
      id: locationsLayer,
      type: "circle",
      source: locationsSource,
      paint: {
        "circle-radius": 5,
        "circle-color": [
          "case",
          ["to-boolean", ["get", "fredNr"]],
          "red",
          "blue",
        ],
      },
    });
  }, [isInitializing]);

  useEffect(() => {
    if (!map.current) {
      map.current = new mapboxgl.Map({
        container: mapContainer.current ?? "",
        style: "mapbox://styles/mapbox/satellite-streets-v11",
        bounds: [
          16.73356562236893,
          58.045415372194725,
          5.32004710530984,
          54.17412229110002,
        ],
        zoom: 7.035819107698143,
      });

      map.current.on("load", () => setIsInitializing(false));
    }
    //     map.current.on("moveend", handleMapChanges);
    //     map.current.on("pitchend", handleMapChanges);
    //     map.current.on("mouseenter", locationsLayer, () => {
    //       const m = map.current;
    //       if (m) {
    //         m.getCanvas().style.cursor = "pointer";
    //       }
    //     });
    //     map.current.on("mouseleave", locationsLayer, () => {
    //       const m = map.current;
    //       if (m) {
    //         m.getCanvas().style.cursor = "";
    //       }
    //     });
    //     map.current.on("click", locationsLayer, (e: any) => {
    //       const m = map.current;

    //       if (m) {
    //         const coordinates = e.features[0].geometry.coordinates;
    //         const location = e.features[0].properties as ILocation;

    //         const popupContent = location
    //           ? `<b>${location.stednavn}</b>
    //         <br />${location.anlaegsbetydning}
    //         <br />${location.datering}
    //         <br /><a href="http://www.kulturarv.dk/fundogfortidsminder/Lokalitet/${location.systemnr}" target="_blank">Fund of fortidsminder</a>`
    //           : "";

    //         new mapboxgl.Popup()
    //           .setLngLat(coordinates)
    //           .setHTML(popupContent)
    //           .addTo(m);
    //       }
    //     });
    //   }
    // }, [onZoomOrBoundsChanged, locationsLayer]);
  }, []);

  return <div ref={(el) => (mapContainer.current = el)} />;
};

export default Map;
