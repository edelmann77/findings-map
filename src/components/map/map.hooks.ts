import { useContext, useEffect, useState } from "react";
import { LocationContext } from "../../contexts/locationContext/locationContext";
import { GeoLocation } from "../types";

export const useFindingsHandler = () => {
  const { state: locationState } = useContext(LocationContext);
  const [geoLocations, setGeoLocations] = useState<GeoLocation[]>([]);

  useEffect(() => {
    const locations: GeoLocation[] = locationState.importedFindings.map(
      (f) => ({
        type: "Feature",
        geometry: { coordinates: [f.lng, f.lat], type: "Point" },
        properties: f,
      })
    );

    setGeoLocations((prev) => [...prev, ...locations]);
  }, [locationState.importedFindings]);

  return { geoLocations };
};
