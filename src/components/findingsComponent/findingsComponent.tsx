import React, { FunctionComponent } from "react";
import { useDataHandler } from "./findingsComponent.hooks";
import "./findingsComponent.css";
import { Draggable } from "react-beautiful-dnd";

const FindingsComponent: FunctionComponent = () => {
  const { importedFindings } = useDataHandler();

  return (
    <div className='FindingsComponent__data'>
      <ul>
        {importedFindings.length ? (
          <>
            <li>Importerede fund</li>
            <ul>
              {importedFindings.map((f, index) => (
                <Draggable
                  key={f.dimeId}
                  draggableId={`${f.dimeId}`}
                  index={index}
                >
                  {(provided) => (
                    <li
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                    >
                      {f.object}
                    </li>
                  )}
                </Draggable>
              ))}
            </ul>
          </>
        ) : undefined}
      </ul>
    </div>
  );
};

export default FindingsComponent;
