import { useContext } from "react";
import { LocationContext } from "../../contexts/locationContext/locationContext";

export const useDataHandler = () => {
  const { state } = useContext(LocationContext);

  return { importedFindings: state.importedFindings };
};
