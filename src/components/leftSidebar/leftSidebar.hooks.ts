import { useCallback, useContext } from "react";
import XLSX from "xlsx";
import proj4 from "proj4";
import { Finding } from "../../contexts/locationContext/locationContext.types";
import { LocationContext } from "../../contexts/locationContext/locationContext";

export const useDataHandler = () => {
  const {
    setImportedFinding,
    addLocation,
    addFindingToLocation,
    removeFinding,
  } = useContext(LocationContext);

  const onPointsUploaded = useCallback(
    (findings: Finding[]) => {
      setImportedFinding(findings);
    },
    [setImportedFinding]
  );

  const handleFindingDropped = useCallback(
    (droppableId: string | undefined, draggableId: string) => {
      console.log(droppableId);
      if (!droppableId || droppableId === "list") {
        return;
      }

      addFindingToLocation(droppableId, draggableId);
      removeFinding(draggableId);
    },
    [addFindingToLocation, removeFinding]
  );

  return {
    onPointsUploaded,
    addLocation,
    handleFindingDropped,
  };
};

export const useXsltHandler = () => {
  const convertToFindings = useCallback(
    async (
      file: File | undefined,
      onPointsUploaded: (findings: Finding[]) => void
    ) => {
      if (file) {
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          const data = e.target?.result;
          if (data) {
            const workbook = XLSX.read(data, { type: "array" });
            const sheetNames = workbook.SheetNames;
            var sheetIndex = 1;
            var df = XLSX.utils.sheet_to_json(
              workbook.Sheets[sheetNames[sheetIndex - 1]]
            );

            const findings: Finding[] = [];

            df.forEach((r: any) => {
              var utm = "+proj=utm +zone=32 +north +no_defs";
              var wgs84 = "+proj=longlat +no_defs";

              const coordinates: number[] = proj4(utm, wgs84, [
                r["Øst"],
                r["Nord"],
              ]);

              findings.push({
                lng: coordinates[0],
                lat: coordinates[1],
                address: r["Adresse"],
                dating: r["Datering"],
                dimeId: r["DimeID"],
                email: r["Mail"],
                finder: r["Finder"],
                material: r["Materiale"],
                matrikel: r["Matr. Nr."],
                object: r["Genstand"],
              });
            });

            onPointsUploaded(findings);
          }
        };
        await fileReader.readAsArrayBuffer(file);
      }
    },
    []
  );

  return { convertToFindings };
};
