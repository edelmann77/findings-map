import React, { FunctionComponent } from "react";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import FindingsComponent from "../findingsComponent/findingsComponent";
import LocationsComponent from "../locationsComponent/locationsComponent";
import "./leftSidebar.css";
import { useDataHandler, useXsltHandler } from "./leftSidebar.hooks";

const LeftSidebar: FunctionComponent = () => {
  const { onPointsUploaded, addLocation, handleFindingDropped } =
    useDataHandler();

  const { convertToFindings } = useXsltHandler();

  return (
    <DragDropContext
      onDragEnd={(result) =>
        handleFindingDropped(
          result.destination?.droppableId,
          result.draggableId
        )
      }
    >
      <Droppable droppableId='list'>
        {(provided) => (
          <div
            className='LeftSidebar__Content'
            ref={provided.innerRef}
            {...provided.droppableProps}
          >
            <div>
              <button onClick={addLocation}>Tilføj lokation</button>
            </div>
            <LocationsComponent />
            <span style={{ fontSize: "13.3333px" }}>
              Tilføj eksportfil fra DIME
            </span>
            <input
              type='file'
              name='Importer'
              onChange={async (e) => {
                convertToFindings(e.target?.files?.[0], onPointsUploaded);
              }}
            />
            {/* <button>Gem</button> */}
            <FindingsComponent />
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  );
};

export default LeftSidebar;
