import React, { FunctionComponent } from "react";
import { Droppable } from "react-beautiful-dnd";
import { Finding } from "../../../contexts/locationContext/locationContext.types";

type LocationFindingProps = {
  finding: Finding;
};

const LocationFindingComponent: FunctionComponent<LocationFindingProps> = ({
  finding,
}) => {
  return (
    <Droppable droppableId={`${finding.dimeId}`}>
      {(provided) => (
        <div ref={provided.innerRef} {...provided.droppableProps}>
          {provided.placeholder}
          {finding.object}
        </div>
      )}
    </Droppable>
  );
};

export default LocationFindingComponent;
