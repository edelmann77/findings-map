import React, { FunctionComponent, useRef, useState } from "react";
import { useLocationsComponentDataHook } from "./locationsComponent.hooks";
import { Droppable } from "react-beautiful-dnd";
import LocationFindingComponent from "./locationFindingComponent/locationFindingComponent";

const LocationsComponent: FunctionComponent = () => {
  const [editLocationId, setEditLocationId] = useState<string>();
  const activeInput = useRef<HTMLInputElement | null>();
  const { changeLocationName, locations } = useLocationsComponentDataHook();

  return locations.length ? (
    <ul>
      {locations.map((location) => (
        <li
          onDoubleClick={() => setEditLocationId(location.id)}
          key={location.id}
        >
          {editLocationId === location.id ? (
            <input
              ref={(r) => (activeInput.current = r)}
              type='text'
              onKeyUp={(e) => {
                if (e.key === "Enter") {
                  activeInput.current?.blur();
                }
              }}
              onBlur={() => setEditLocationId(undefined)}
              onChange={(e) => changeLocationName(location.id, e.target.value)}
              value={location.name}
            />
          ) : (
            <>
              <Droppable droppableId={location.id}>
                {(provided) => (
                  <div ref={provided.innerRef} {...provided.droppableProps}>
                    {location.name}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>

              {location.findings?.length ? (
                <ul>
                  {location.findings.map((f) => (
                    <li key={f.dimeId}>
                      <LocationFindingComponent finding={f} />
                    </li>
                  ))}
                </ul>
              ) : undefined}
            </>
          )}
        </li>
      ))}
    </ul>
  ) : (
    <div />
  );
};

export default LocationsComponent;
