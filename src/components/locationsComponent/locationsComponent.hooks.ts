import { useContext } from "react";
import { LocationContext } from "../../contexts/locationContext/locationContext";

export const useLocationsComponentDataHook = () => {
  const { changeLocationName, state } = useContext(LocationContext);
  return {
    locations: state.locations,
    changeLocationName,
  };
};
