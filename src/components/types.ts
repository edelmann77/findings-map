import { Feature, Geometry } from "geojson";

export type GeoLocation = Feature<Geometry, {}>;
