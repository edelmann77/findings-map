import React, {
  createContext,
  FunctionComponent,
  useReducer,
  useCallback,
} from "react";
import { locationReducer } from "./locationContext.reducer";
import {
  Finding,
  LocationContextState,
  LocationContextType,
} from "./locationContext.types";

const initialState: LocationContextState = {
  importedFindings: [],
  locations: [],
};

export const LocationContext = createContext<LocationContextType>({
  state: initialState,
  setImportedFinding: () => null,
  addLocation: () => null,
  changeLocationName: () => null,
  addFindingToLocation: () => null,
  removeFinding: () => null,
});

export const LocationProvider: FunctionComponent = ({ children }) => {
  const [state, dispatch] = useReducer(locationReducer, initialState);

  const setImportedFinding = useCallback((findings: Finding[]) => {
    dispatch({ type: "setImportFindings", findings });
  }, []);

  const addLocation = useCallback(() => {
    dispatch({ type: "addLocation" });
  }, []);

  const changeLocationName = useCallback((locationId: string, name: string) => {
    dispatch({ type: "setLocationName", locationId, name });
  }, []);

  const addFindingToLocation = useCallback((locationId, findingId) => {
    dispatch({ type: "addFindingToLocation", locationId, findingId });
  }, []);

  const removeFinding = useCallback((findingId: string) => {
    dispatch({ type: "removeFromFindings", findingId });
  }, []);

  return (
    <LocationContext.Provider
      value={{
        state,
        setImportedFinding,
        addLocation,
        changeLocationName,
        addFindingToLocation,
        removeFinding,
      }}
    >
      {children}
    </LocationContext.Provider>
  );
};
