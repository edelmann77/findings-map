export type Finding = {
  address: string;
  dating: string;
  dimeId: number;
  finder: string;
  object: string;
  email: string;
  material: string;
  matrikel: string;
  lng: number;
  lat: number;
};

export type Location = {
  id: string;
  name: string;
  findings: Finding[];
};

export type LocationContextState = {
  importedFindings: Finding[];
  locations: Location[];
};

export type LocationContextAction =
  | {
      type: "setImportFindings";
      findings: Finding[];
    }
  | {
      type: "addLocation";
    }
  | {
      type: "setLocationName";
      locationId: string;
      name: string;
    }
  | {
      type: "addFindingToLocation";
      locationId: string;
      findingId: string;
    }
  | {
      type: "removeFromFindings";
      findingId: string;
    };

export type LocationContextType = {
  state: LocationContextState;
  setImportedFinding: (f: Finding[]) => void;
  addLocation: () => void;
  changeLocationName: (locationId: string, name: string) => void;
  addFindingToLocation: (locationId: string, findingId: string) => void;
  removeFinding: (findingId: string) => void;
};
