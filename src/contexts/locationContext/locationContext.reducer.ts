import {
  LocationContextState,
  LocationContextAction,
} from "./locationContext.types";

import { uniqueId } from "lodash";

export const locationReducer = (
  state: LocationContextState,
  action: LocationContextAction
): LocationContextState => {
  switch (action.type) {
    case "setImportFindings": {
      return { ...state, importedFindings: action.findings };
    }
    case "addLocation": {
      return {
        ...state,
        locations: [
          ...state.locations,
          { id: uniqueId(), name: "Ny lokation", findings: [] },
        ],
      };
    }
    case "setLocationName": {
      const location = state.locations.find((l) => l.id === action.locationId);
      if (location) {
        location.name = action.name;
        return { ...state, locations: [...state.locations] };
      }
      return state;
    }
    case "addFindingToLocation": {
      const location = state.locations.find((l) => l.id === action.locationId);
      const finding = state.importedFindings.find(
        (f) => `${f.dimeId}` === action.findingId
      );
      if (location && finding) {
        location.findings.push(finding);
        return { ...state, locations: [...state.locations] };
      }
      return state;
    }
    case "removeFromFindings": {
      const findings = state.importedFindings.filter(
        (f) => `${f.dimeId}` !== action.findingId
      );
      return { ...state, importedFindings: findings };
    }
    default: {
      return state;
    }
  }
};
