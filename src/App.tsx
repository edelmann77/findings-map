import "./App.css";
import React from "react";
import LeftSidebar from "./components/leftSidebar/leftSidebar";
import Map from "./components/map/map";
import { LocationProvider } from "./contexts/locationContext/locationContext";

function App() {
  return (
    <LocationProvider>
      <Map />
      <div className='App__LeftSidebar'>
        <LeftSidebar />
      </div>
    </LocationProvider>
  );
}

export default App;
